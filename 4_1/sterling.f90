program  sterling
implicit none
integer :: i
double precision :: fact

open (unit = 2, file = "data")

do i=1,100
  write(2,*) i,100*(dlog(fact(i))-(dble(i)*dlog(dble(i))-dble(i)))/dlog(fact(i))
enddo

end

double precision function fact(n)
implicit none
integer, intent(in) :: n
integer :: i
double precision :: ans

ans = 1
do i = 1, n
  ans = ans * dble(i)
enddo
fact = ans

end function fact


