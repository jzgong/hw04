program poly
implicit None
double precision,allocatable,dimension(:) :: x, y
integer :: i,n,d

interface 
  subroutine poly_fit(x, y, d)
  implicit none
  integer, intent(in) :: d
  double precision, intent(in), dimension(:) :: x, y
  end subroutine poly_fit
end interface

open(unit=2,file="data2")
  read(2,*) d          !dimension of fit
  read(2,*) n          !number of points
  allocate(x(n),y(n))

  do i=1,n
    read(2,*) x(i),y(i)   !data points
  enddo
close(2)

call poly_fit(x,y,d)

end program poly



subroutine poly_fit(x,y,d)
implicit none
integer, intent(in) :: d
double precision, dimension(:), intent(in) :: x, y
double precision, allocatable, dimension(:,:) :: xx, xx_e, A, Ainv
double precision, allocatable, dimension(:) :: work, c
integer, dimension(:), allocatable :: ipiv
integer :: m, n, l, lda, lwork, info

l=d+1

allocate(c(l))
allocate(ipiv(l))
allocate(work(l))
allocate(xx(size(x),l))
allocate(xx_e(size(x),l))
allocate(A(l,l),Ainv(l,l))

do n = 1,size(x)
do m = 1,l
  xx(n,m) = x(n)**(m-1)         !polynomial matrix
  xx_e(n,m)= dexp(-(m-1)*x(n))  !expoential matrix
end do
end do

A = matmul(transpose(xx_e),xx_e)  !changes depending on type of fit
Ainv = A

call dgetrf(l, l, Ainv, l, ipiv, info)
call dgetri(l, Ainv, l, ipiv, work, l, info)

c = matmul(matmul(Ainv,transpose(xx_e)),y) !changes on type of fit
print *, c     !fit coefficients

end subroutine poly_fit
