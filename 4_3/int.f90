program int
implicit none
integer :: i,j,N
double precision :: int_trap,int_simp

do i=1,5

N=2**i

call integration(N,int_trap,int_simp)

print *, N, (0.842700792-int_trap)/0.842700792, abs(0.842700792-int_simp)/0.842700792

enddo

end



subroutine integration(N,int_trap,int_simp)
implicit none
integer, intent(in) :: N
double precision, dimension(N+1) :: x1
double precision, dimension(2*N+1) :: x2
double precision :: pi, sum1, sum2
double precision, intent(out) :: int_trap,int_simp
integer :: i

pi=3.14159265359
sum1=0
sum2=0

!Trapazoidal Rule
do i=1,N+1
  x1(i)=exp(-(real(i-1)/real(N))**2)
  if (i.ne.1 .and. i.ne.N+1) then
    x1(i)=2*x1(i)
  endif
  sum1=sum1+x1(i)
enddo

int_trap=1/(sqrt(pi)*real(N))*sum1

!Simpson's Rule
do i=1,2*N+1
  x2(i)=exp(-(real(i-1)/real(2*N))**2)
  if (mod(i,2).eq.0) then
    x2(i)=4*x2(i)
  elseif (i.ne.1 .and. i.ne.2*N+1 .and. mod(i,2).eq.1) then
    x2(i)=2*x2(i)
  endif 
  sum2=sum2+x2(i)
enddo

int_simp=1/(3*sqrt(pi)*real(N))*sum2



end subroutine integration
